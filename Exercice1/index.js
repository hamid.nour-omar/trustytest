const express = require('express')
const fetch = require('node-fetch')

const PORT = process.env.PORT || 3000

const app = express()

app.use(express.json())
app.use(express.urlencoded({extended: true}))

function generate(json, companyName, request) {
    return {
        companyName:companyName,
        endpointLever: request,
        nbrJobs: json.length,
        openJobs : json
    }
}

app.get('/getOpenJobsInfos/:companyName', async (req, res) => {
    const companyName = req.params.companyName
    const request = `https://api.lever.co/v0/postings/${companyName}?mode=json`
    const jsonP = await fetch(request)
    const json = await jsonP.json()
    const result = generate(json, companyName, request)
    
    res.status(200).send(result)
})

app.listen(PORT, () => {
    console.log(`App is running on port ${PORT}`)
})