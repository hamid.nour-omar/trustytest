const cron = require('cron').CronJob

function generate(companyName, json, message) {
    return {
        companyName:companyName,
        broadcastSlackMessage: message,
        nbrOpenJobThisWeek: json.length
    }
}

function newMessage(json, companyName) {

}

async function retrieve({companyName, message})  {
    const request = `https://api.lever.co/v0/postings/${companyName}?mode=json`
    const jsonP = await fetch(request)
    const json = await jsonP.json()
    message = newMessage(json, companyName)
    const result = generate(json, companyName, message)
    return result

}

let [Swile, Backmarket, Ledger] = [
    {
        company: "Swile",
        message: ''
    },
    {
        company: "Backmarket",
        message: ''
    },
    {
        company: "Ledger",
        message: ''
    }
] 

const companies = [Swile, Backmarket, Ledger]
let result = []

const job = new cron('0 0 * * 1', ()=>{
    companies.forEach(element => {
        result.push(retrieve(element))
    });

}, null, true, 'Europe/Paris' )